﻿using System;
using System.Text;
using static System.Math;
namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна №2.\nВиконав: Тишкевич Н.Ю\nВаріант №6\nЗавдання 3.");
            double N, res = 0;
            do
            {
                Console.WriteLine("N = ");
                if (double.TryParse(Console.ReadLine(), out N)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            for (int i = 0; i < N; i++)
            {
                double j = N + 1;
                res = res + Math.Pow(i, --j);
                Console.WriteLine($"Result = {res}\n");
            }
        }
    }
}

