﻿using System;
using System.Text;
using static System.Math;
namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна №2.\nВиконав: Тишкевич Н.Ю\nВаріант №16\nЗавдання 3.");
            double N, res = 0, K;
            do
            {
                Console.WriteLine("N =");
                if (double.TryParse(Console.ReadLine(), out N)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            do
            {
                Console.WriteLine("K =");
                if (double.TryParse(Console.ReadLine(), out K)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            for (int i = 0; i < N; i++)
            {
                res += Math.Pow(i,K);
                Console.WriteLine($"Result = {res}\n");
            }
        }
    }
}
