﻿using System;
using System.Text;
using static System.Math;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
        
            Console.WriteLine("Лабораторна №2.\nВиконав: Тишкевич Н.Ю\nВаріант №6\nЗавдання 2.");
            double a,b,c,D,x1,x2;
            do
            {
                Console.Write("Введіть значення a = ");
                if (double.TryParse(Console.ReadLine(), out a)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            do
            {

                Console.Write("Введіть значення b = ");
                if (double.TryParse(Console.ReadLine(), out b)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            do
            {

                Console.Write("Введіть значення c = ");
                if (double.TryParse(Console.ReadLine(), out c)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            D = Math.Pow(b, 2) - 4 * a * c;
            if(D>0)
            {
                x1 = (-b + Math.Sqrt(D)) / (2 * a);
                x2 = (-b - Math.Sqrt(D)) / (2 * a);
                if(a==0)
                {
                    Console.WriteLine("Помилка ділення на нуль!");
                }
                else
                {
                    Console.WriteLine($"Дискримінант більший за нуль: D = {D}\nРівняння має два корені : x1 = {x1}, x2 = {x2}");
                }
            }
            if(D==0)
            {
                x1 = -b / (2 * a);
                if(a==0)
                {
                    Console.WriteLine("Помилка ділення на нуль!");
                }
                else
                {
                    Console.WriteLine($"Дискримінант дорівнює нулю: D = {D}\nРівняння має один корінь : x1 = {x1:F0}");
                }
            }
            if(D<0)
            {
                Console.WriteLine($"Дискримінант менший за нуль: D = {D}\nРівняння не має коренів");
            }
        }
    }
}

