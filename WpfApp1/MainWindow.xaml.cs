﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Label_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void Label_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
        private void Label_Loaded_2(object sender, RoutedEventArgs e)
        {

        }
        private void Label_Loaded_3(object sender, RoutedEventArgs e)
        {

        }
        private void TextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void TextBox2_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }
        private void TextBox3_TextChanged_2(object sender, TextChangedEventArgs e)
        {

        }
        private void TextBox4_TextChanged_3(object sender, TextChangedEventArgs e)
        {

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z, s;
            if (double.TryParse(TextBox1.Text, out x)) { }
            else
            {
                MessageBox.Show("Помилка!", "Спробуйте ще!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (double.TryParse(TextBox2.Text, out y)) { }
            else
            {
                MessageBox.Show("Помилка!", "Спробуйте ще!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (double.TryParse(TextBox3.Text, out z)) { }
            else
            {
                MessageBox.Show("Помилка!", "Спробуйте ще!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            s = Math.Sqrt(10 * (Math.Sqrt(x) + Math.Pow(x, y + 2))) * Math.Sin(z - Math.Abs(x + y));
            TextBox4.Text = s.ToString("F2");
        }

    }
}


      