﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void TextBoxa_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBoxb_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBoxc_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBoxx1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBoxx2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void Лабораторна__2__Завдання_2_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, c, D, x1, x2;
            if(double.TryParse(TextBoxa.Text,out a)) { }
            else
            {
                MessageBox.Show("Помилка а!", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (double.TryParse(TextBoxb.Text, out b)) { }
            else
            {
                MessageBox.Show("Помилка b!", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (double.TryParse(TextBoxc.Text, out c)) { }
            else
            {
                MessageBox.Show("Помилка c!", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            D = Math.Pow(b, 2) - 4 * a * c;
            if(D>0)
            {
                x1 = (-b + Math.Sqrt(D)) / (2 * a);
                x2 = (-b - Math.Sqrt(D)) / (2 * a);
                if(a==0)
                {
                    MessageBox.Show("Помилка ділення на нуль!", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    TextBoxD.Text = D.ToString("F");
                    label5.Visibility = Visibility.Visible;
                    TextBoxx1.Visibility = Visibility.Visible;
                    TextBoxx1.Text = x1.ToString("F");
                    label6.Visibility = Visibility.Visible;
                    TextBoxx2.Visibility = Visibility.Visible;
                    TextBoxx2.Text = x2.ToString("F");

                }
            }
            if(D==0)
            {
                x1 = -b / (2 * a);
                if(a==0)
                {
                    MessageBox.Show("Помилка ділення на нуль!", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else 
                {
                    TextBoxD.Text = D.ToString("F");
                    label5.Visibility = Visibility.Visible;
                    TextBoxx1.Visibility = Visibility.Visible;
                    TextBoxx1.Text = x1.ToString("F");
                    label6.Visibility = Visibility.Hidden;
                    TextBoxx2.Visibility = Visibility.Hidden;
                }
            }
            if(D<0)
            {
                TextBoxD.Text = D.ToString("F");
                label5.Visibility = Visibility.Hidden;
                TextBoxx1.Visibility = Visibility.Hidden;
                label6.Visibility = Visibility.Hidden;
                TextBoxx2.Visibility = Visibility.Hidden;
                MessageBox.Show("Рівняння не має коренів!", "Результат", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

       
    }
}
