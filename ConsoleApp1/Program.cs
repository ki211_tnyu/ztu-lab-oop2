﻿using System;
using System.Text;
using static System.Math;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна №2.\nВиконав: Тишкевич Н.Ю\nВаріант №6\nЗавдання 1.");
            double x, y, z, s;
            do
            {
                Console.Write("Введіть дробове значення х = ");
                if (double.TryParse(Console.ReadLine(), out x)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            do
            {

                Console.Write("Введіть дробове значення y = ");
                if (double.TryParse(Console.ReadLine(), out y)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            do
            {

                Console.Write("Введіть дробове значення z = ");
                if (double.TryParse(Console.ReadLine(), out z)) break;
                else
                {
                    Console.WriteLine("Помилка! Спробуйте ще раз!");
                }
            }
            while (true);
            s = Math.Sqrt(10 * (Math.Sqrt(x) + Math.Pow(x, y + 2))) * Math.Sin(z - Math.Abs(x + y));
            Console.WriteLine($"Результат обчислення : s = {s:F3}");
        }
    }
}
