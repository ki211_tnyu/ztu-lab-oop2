﻿using System;
using System.Text;
using static System.Math;
namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна №2.\nВиконав: Тишкевич Н.Ю\nВаріант №16\nЗавдання 3.");
            double N;
            bool K = false;
            int countPar = 0, countNePar = 0, countPos = 0, countNeg = 0;
            do
            {
                do
                {
                    Console.WriteLine("N =");
                    if (double.TryParse(Console.ReadLine(), out N)) break;
                    else
                    {
                        Console.WriteLine("Помилка! Спробуйте ще раз!");
                    }
                }
                while (true);
                if (N % 2 == 0)
                {
                    countPar = countPar + 1;
                }
                if (N % 2 == 1)
                {
                    countNePar = countNePar + 1;
                }
                if (N > 0)
                {
                    countPos = countPos + 1;
                }
                if (N < 0)
                {
                    countNeg = countNeg + 1;
                }
                else if (N == 0)
                {
                    K = true;
                }
            }
            while (!K);
            Console.WriteLine($"Парні = {countPar}\n");
            Console.WriteLine($"Непарні = {countNePar}\n");
            Console.WriteLine($"Додатні = {countPos}\n");
            Console.WriteLine($"Від'ємні = {countNeg}\n");
        }
    }
}
