namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a,b,c,D,x1,x2;
            if (double.TryParse(textBox1.Text, out a)) { }
            else
            {
                MessageBox.Show("�������!", "��������� ��!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (double.TryParse(textBox2.Text, out b)) { }
            else
            {
                MessageBox.Show("�������!", "��������� ��!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (double.TryParse(textBox3.Text, out c)) { }
            else
            {
                MessageBox.Show("�������!", "��������� ��!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            D = Math.Pow(b, 2) - 4 * a * c;
            if (D > 0)
            {
                x1 = (-b + Math.Sqrt(D)) / (2 * a);
                x2 = (-b - Math.Sqrt(D)) / (2 * a);
                if (a == 0)
                {
                    MessageBox.Show("������� ������ �� ����!", "��������� ��!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    textBox4.Text = D.ToString("F");
                    label5.Visible = true;
                    textBox5.Visible = true;
                    textBox5.Text = x1.ToString("F");
                    label6.Visible = true;
                    textBox6.Visible = true;
                    textBox6.Text = x2.ToString("F");
                }
            }
            if (D == 0)
            {
                x1 = -b / (2 * a);
                if (a == 0)
                {
                    MessageBox.Show("������� ������ �� ����!", "��������� ��!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    textBox4.Text = D.ToString("F");
                    label5.Visible = true;
                    textBox5.Visible = true;
                    textBox5.Text = x1.ToString("F");
                    label6.Visible = false;
                    textBox6.Visible = false;
                }
            }
            if (D < 0)
            {
                textBox4.Text = D.ToString("F");
                label5.Visible = false;
                textBox5.Visible = false;
                label6.Visible = false;
                textBox6.Visible = false;
                MessageBox.Show("г������ �� �� �������!", "��������� ��!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}